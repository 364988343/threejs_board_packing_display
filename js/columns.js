var cartonColumns = [{
			display: '编码',
			name: 'cartonCode',
			width: 30
		},
		{
			display: '长度',
			name: 'length',
			width: 50
		},
		{
			display: '宽度',
			name: 'width',
			width: 50
		},
		{
			display: '高度',
			name: 'height',
			width: 50
		},
		{
			display: '板材数量',
			name: 'count',
			width: 50
		},
		{
			display: '总重量',
			name: 'weight',
			width: 70
		},
		{
			display: '利用率',
			name: 'ratio',
			width: 70
		},
	]

var detailColumns = [{
			display: '编码',
			name: 'partId',
			mintWidth: 30,
			width: 30
		},
		{
			display: '长度',
			name: 'length',
			mintWidth: 25,
			width: 50
		},
		{
			display: '宽度',
			name: 'width',
			mintWidth: 25,
			width: 50
		},
		{
			display: '高度',
			name: 'height',
			mintWidth: 25,
			width: 50
		},
		{
			display: 'X轴',
			name: 'partX',
			mintWidth: 25,
			width: 50
		},
		{
			display: 'Y轴',
			name: 'partY',
			mintWidth: 25,
			width: 50
		},
		{
			display: 'Z轴',
			name: 'partZ',
			mintWidth: 25,
			width: 50
		},
		{
			display: '旋转标识',
			name: 'r',
			mintWidth: 25,
			width: 50
		},
		{
			display: '包装规则',
			name: 'rule',
			mintWidth: 50,
			width: 100
		},
		{
			display: '混包规则',
			name: 'rule',
			mintWidth: 50,
			width: 100
		},
	]
