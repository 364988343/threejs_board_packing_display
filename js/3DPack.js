var renderer, camera, scene, controls, raycaster, axis

var intersects = [],
	o = [],
	intersected, id, index

var cartonLength = 0

var init = function() {
	var frame = document.getElementById('canvas-frame')
	width = frame.clientWidth
	height = frame.clientHeight
	renderer = new THREE.WebGLRenderer({
		antialias: true,
	})
	raycaster = new THREE.Raycaster()
	mouse = new THREE.Vector3()
	document.addEventListener('click', onMouseClick, false)
	renderer.setSize(width, height)
	frame.appendChild(renderer.domElement)
	renderer.setClearColor(0xEDEDED)
	camera = new THREE.PerspectiveCamera(60, width / height, 1, 10000)
	camera.position.x = 1500
	camera.position.y = 1500
	camera.position.z = 1500
	camera.up.x = 0
	camera.up.y = 1
	camera.up.z = 0
	camera.lookAt({
		x: 0,
		y: 0,
		z: 0
	})
	scene = new THREE.Scene()
	controls = new THREE.OrbitControls(camera, renderer.domElement)
	raycaster.intersectObjects(scene.children)
}

var animation = function() {
	requestAnimationFrame(animation)
	controls.update()
	render()
}

var render = function() {
	renderer.render(scene, camera)
}

var clear = function() {
	if(o.length != 0) {
		o = []
	}
}

var initBox = function(length, width, height) {
	var wideFrame = createCube(width / 2, height / 2, length / 2)
	var material = new THREE.LineDashedMaterial({
		color: 0x000000,
		dashSize: 20,
		gapSize: 2,
		linewidth: 1
	})
	mesh = new THREE.LineSegments(wideFrame, material)
	mesh.position.set(width / 2, height / 2, length / 2)
	scene.add(mesh)
	return mesh
}

var initAxis = function() {
	axis = new THREE.AxisHelper(2000)
	axis.position.set(0, 0, 0)
	scene.add(axis)
}

var createCube = function(x, y, z) {
	var geometry = new THREE.Geometry()

	geometry.vertices.push(
		new THREE.Vector3(-x, -y, -z),
		new THREE.Vector3(-x, y, -z),

		new THREE.Vector3(-x, y, -z),
		new THREE.Vector3(x, y, -z),

		new THREE.Vector3(x, y, -z),
		new THREE.Vector3(x, -y, -z),

		new THREE.Vector3(x, -y, -z),
		new THREE.Vector3(-x, -y, -z),

		new THREE.Vector3(-x, -y, z),
		new THREE.Vector3(-x, y, z),

		new THREE.Vector3(-x, y, z),
		new THREE.Vector3(x, y, z),

		new THREE.Vector3(x, y, z),
		new THREE.Vector3(x, -y, z),

		new THREE.Vector3(x, -y, z),
		new THREE.Vector3(-x, -y, z),

		new THREE.Vector3(-x, -y, -z),
		new THREE.Vector3(-x, -y, z),

		new THREE.Vector3(-x, y, -z),
		new THREE.Vector3(-x, y, z),

		new THREE.Vector3(x, y, -z),
		new THREE.Vector3(x, y, z),

		new THREE.Vector3(x, -y, -z),
		new THREE.Vector3(x, -y, z)
	)
	return geometry
}

var colorMap = {}
var getColor = function(cubeName) {
	if(cubeName == 0) {
		var color = 0xFF0000
	} else {
		if(colorMap && colorMap[cubeName])
			return colorMap[cubeName]
		var color = parseInt((Math.random() * 0xFFFFCC).toString(16), 16)
		if(color == 0xFF0000)
			color = parseInt((Math.random() * 0xFFFFCC).toString(16), 16)

		if(color == 0x000000)
			color = parseInt((Math.random() * 0xFFFFCC).toString(16), 16)

		if(color == 0xCCCCCC)
			color = parseInt((Math.random() * 0xFFFFCC).toString(16), 16)
	}
	return color
}

var initObject = function(width, height, length, x, y, z, cubeName) {
	mesh = new THREE.Object3D()
	var geometry = new THREE.BoxGeometry(width, height, length)
	var material = new THREE.MeshBasicMaterial({
		color: getColor(cubeName)
	})
	var cube = new THREE.Mesh(geometry, material)
	mesh.add(cube)
	var wideFrame = createCube(width / 2, height / 2, length / 2)
	var material = new THREE.LineDashedMaterial({
		color: 0x000000,
		dashSize: 20,
		gapSize: 2,
		linewidth: 1
	})
	var lineFrame = new THREE.LineSegments(wideFrame, material)
	mesh.add(lineFrame)
	mesh.position.set(y + width / 2, z + height / 2, x + length / 2)
	scene.add(mesh)
	o.push(cube)
	return mesh
}

var onMouseClick = function(event) {
	event.preventDefault()
	mouse.x = (event.clientX / (window.innerWidth - 312)) * 2 - 1
	mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
	raycaster.setFromCamera(mouse, camera)
	intersections = raycaster.intersectObjects(o)
	if(intersections.length > 0) {
		if(intersected != intersections[0].object) {
			if(intersected) {
				$("tr").removeClass("l-selected")
				intersected.material.color.getHex() == 0x000001 ? intersected.material.color.setHex(0xFF0000) : intersected.material.color.setHex(getColor())
			}
			intersected = intersections[0].object
			intersected.material.color.getHex() == 0xFF0000 ? intersected.material.color.setHex(0x000001) : intersected.material.color.setHex(0x000000)
			for(var i = o.length - 1; i >= 0; i--) {
				if(o[i].id === intersected.id) {
					index = i
				}
			}
			$(".tab-title-li").removeClass("on")
			$($(".tab-title-li")[3]).addClass("on")
			$(".tab-body-li").addClass("dn")
			$($(".tab-body-li")[3]).removeClass("dn")
			$($("tr")[(($("tr").length - o.length) + index)]).addClass("l-selected")
		}
	} else if(intersected) {
		$("tr").removeClass("l-selected")
		intersected.material.color.getHex() == 0x000001 ? intersected.material.color.setHex(0xFF0000) : intersected.material.color.setHex(getColor())
		intersected = null
	}
}